#ifndef NIFTI2_HEADER_HPP
#define NIFTI2_HEADER_HPP

#include <array>
#include <cstdint>

namespace nifti2
{
struct header
{
private:
  std::int32_t                         header_size;           // Redundant.
  std::array<char, 8>                  magic_number;          // Redundant.
public:                                                       
  std::int16_t                         data_type;             
  std::int16_t                         bits_per_voxel;        
  std::array<std::int64_t, 8>          dimensions;            
  std::array<double, 3>                intent;                // Redundant.
  std::array<double, 8>                spacing;               
private:                                                      
  std::int64_t                         data_offset;           // Redundant.
public:                                                       
  double                               scaling_multiplier;    // Redundant.
  double                               scaling_addition;      // Redundant.
  double                               maximum_intensity;     // Redundant.
  double                               minimum_intensity;     // Redundant.
  double                               time_spacing;          // Redundant.
  double                               time_offset;           
  std::array<std::int64_t, 2>          slice_range;           
  std::array<char, 80>                 description;           
  std::array<char, 24>                 auxiliary_file;        // Redundant.
  std::int32_t                         q_form_code;           // Redundant.
  std::int32_t                         s_form_code;           // Redundant.
  std::array<double, 3>                q_form_quaternion_bcd; 
  std::array<double, 3>                q_form_offset;         
  std::array<std::array<double, 4>, 3> s_form_matrix;         
  std::int32_t                         slice_code;            // Redundant.
  std::int32_t                         units;
  std::int32_t                         intent_code;           // Redundant.
  std::array<char, 16>                 intent_name;           // Redundant.
  char                                 slice_ordering;
private:                                                      
  std::array<char, 15>                 unused;                // Redundant.
};
}

#endif