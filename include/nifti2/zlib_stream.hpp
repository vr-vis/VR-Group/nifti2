#ifndef NIFTI2_ZLIB_STREAM_HPP
#define NIFTI2_ZLIB_STREAM_HPP

#include <cstddef>
#include <cstdint>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <zlib.h>

namespace nifti2::zlib
{
class exception : public std::exception
{
public:
  explicit exception  (const std::string& message) : message_(message)
  {
    
  }
  exception           (z_stream* stream, const std::int32_t code) : message_("ZLIB ")
  {
    switch (code)
    {
    case Z_STREAM_ERROR:
      message_ += "Z_STREAM_ERROR: ";
      break;
    case Z_DATA_ERROR:
      message_ += "Z_DATA_ERROR: ";
      break;
    case Z_MEM_ERROR:
      message_ += "Z_MEM_ERROR: ";
      break;
    case Z_VERSION_ERROR:
      message_ += "Z_VERSION_ERROR: ";
      break;
    case Z_BUF_ERROR:
      message_ += "Z_BUF_ERROR: ";
      break;
    default:
      std::ostringstream oss;
      oss << code;
      message_ += "[" + oss.str() + "]: ";
      break;
    }
    message_ += stream->msg;
  }
  exception           (const exception&  that) = default;
  exception           (      exception&& temp) = default;
  virtual ~exception  ()                       = default;
  exception& operator=(const exception&  that) = default;
  exception& operator=(      exception&& temp) = default;

  const char* what() const noexcept override
  {
    return message_.c_str();
  }
  
private:
  std::string message_;
};

class stream_wrapper : public z_stream
{
public:
  explicit stream_wrapper  (const bool is_input = true, const std::int32_t level = Z_DEFAULT_COMPRESSION) : z_stream_s(), is_input_(is_input)
  {
    zalloc = nullptr;
    zfree  = nullptr;
    opaque = nullptr;

    std::int32_t code;
    if (is_input_)
    {
      avail_in = 0;
      next_in  = nullptr;
      code     = inflateInit2(this, 15 + 32);
    }
    else
      code     = deflateInit2(this, level, Z_DEFLATED, 15 + 16, 8, Z_DEFAULT_STRATEGY);

    if (code != Z_OK)
      throw exception(this, code);
  }
  stream_wrapper           (const stream_wrapper&  that) = delete;
  stream_wrapper           (      stream_wrapper&& temp) = delete;
  virtual ~stream_wrapper  ()
  {
    if (is_input_)
      inflateEnd(this);
    else
      deflateEnd(this);
  }
  stream_wrapper& operator=(const stream_wrapper&  that) = delete;
  stream_wrapper& operator=(      stream_wrapper&& temp) = delete;

private:
  bool is_input_;
};

class istreambuf : public std::streambuf
{
public:
  explicit istreambuf  (std::streambuf* stream_buffer, const std::size_t buffer_size = static_cast<std::size_t>(1) << 20, const bool auto_detect = true)
  : stream_buffer_     (stream_buffer)
  , buffer_size_       (buffer_size)
  , auto_detect_       (auto_detect)
  , input_buffer_      (new char[buffer_size])
  , input_buffer_start_(input_buffer_)
  , input_buffer_end_  (input_buffer_)
  , output_buffer_     (new char[buffer_size])
  , stream_wrapper_    (nullptr)
  , auto_detect_run_   (false)
  , is_text_           (false)
  {
    setg(output_buffer_, output_buffer_, output_buffer_);
  }
  istreambuf           (const istreambuf&  that) = delete ;
  istreambuf           (      istreambuf&& temp) = default;
  virtual ~istreambuf  ()
  {
    delete[] input_buffer_  ;
    delete[] output_buffer_ ;
    delete   stream_wrapper_;
  }
  istreambuf& operator=(const istreambuf&  that) = delete ;
  istreambuf& operator=(      istreambuf&& temp) = default;

  std::streambuf::int_type underflow() override
  {
    if (gptr() == egptr())
    {
      auto output_buffer_free_start = output_buffer_;
      do
      {
        if (input_buffer_start_ == input_buffer_end_)
        {
          input_buffer_start_ = input_buffer_;
          const auto size     = stream_buffer_->sgetn(input_buffer_, buffer_size_);
          input_buffer_end_   = input_buffer_ + size;
          if (input_buffer_end_ == input_buffer_start_)
            break;
        }
        if (auto_detect_ && !auto_detect_run_)
        {
          auto_detect_run_ = true;
          const auto b0 = *reinterpret_cast<std::uint8_t*>(input_buffer_start_);
          const auto b1 = *reinterpret_cast<std::uint8_t*>(input_buffer_start_ + 1);
          is_text_ = !(input_buffer_start_ + 2 <= input_buffer_end_ && (b0 == 0x1F && b1 == 0x8B || b0 == 0x78 && (b1 == 0x01 || b1 == 0x9C || b1 == 0xDA)));
        }
        if (is_text_)
        {
          std::swap(input_buffer_, output_buffer_);
          output_buffer_free_start = input_buffer_end_;
          input_buffer_start_      = input_buffer_;
          input_buffer_end_        = input_buffer_;
        }
        else
        {
          if (!stream_wrapper_)
            stream_wrapper_ = new stream_wrapper(true);

          stream_wrapper_->next_in   = reinterpret_cast<decltype(stream_wrapper_->next_in)> (input_buffer_start_);
          stream_wrapper_->avail_in  = input_buffer_end_ - input_buffer_start_;
          stream_wrapper_->next_out  = reinterpret_cast<decltype(stream_wrapper_->next_out)>(output_buffer_free_start);
          stream_wrapper_->avail_out = output_buffer_ + buffer_size_ - output_buffer_free_start;

          const auto code = inflate(stream_wrapper_, Z_NO_FLUSH);
          if (code != Z_OK && code != Z_STREAM_END)
            throw exception(stream_wrapper_, code);
          
          input_buffer_start_      = reinterpret_cast<decltype(input_buffer_start_)>(stream_wrapper_->next_in);
          input_buffer_end_        = input_buffer_start_ + stream_wrapper_->avail_in;
          output_buffer_free_start = reinterpret_cast<decltype(output_buffer_free_start)>(stream_wrapper_->next_out);
          
          if (code == Z_STREAM_END)
          {
            delete stream_wrapper_;
            stream_wrapper_ = nullptr;
          }
        }
      } while (output_buffer_free_start == output_buffer_);
      setg(output_buffer_, output_buffer_, output_buffer_free_start);
    }
    return gptr() == egptr() ? traits_type::eof() : traits_type::to_int_type(*gptr());
  }

private:
  std::streambuf* stream_buffer_     ;
  std::size_t     buffer_size_       ;
  bool            auto_detect_       ;

  char*           input_buffer_      ;
  char*           input_buffer_start_;
  char*           input_buffer_end_  ;
  char*           output_buffer_     ;
  stream_wrapper* stream_wrapper_    ;
  bool            auto_detect_run_   ;
  bool            is_text_           ;
};
class ostreambuf : public std::streambuf
{
public:
  explicit ostreambuf  (std::streambuf* stream_buffer, const std::size_t buffer_size = static_cast<std::size_t>(1) << 20, const std::int32_t level = Z_DEFAULT_COMPRESSION)
  : stream_buffer_ (stream_buffer)
  , buffer_size_   (buffer_size)
  , input_buffer_  (new char[buffer_size])
  , output_buffer_ (new char[buffer_size])
  , stream_wrapper_(new stream_wrapper(false, level))
  {
    setp(input_buffer_, input_buffer_ + buffer_size_);
  }
  ostreambuf           (const ostreambuf&  that) = delete ;
  ostreambuf           (      ostreambuf&& temp) = default;
  virtual ~ostreambuf  ()
  {
    ostreambuf::sync();
    delete[] input_buffer_  ;
    delete[] output_buffer_ ;
    delete   stream_wrapper_;
  }
  ostreambuf& operator=(const ostreambuf&  that) = delete ;
  ostreambuf& operator=(      ostreambuf&& temp) = default;

  std::int32_t             deflate_loop(const std::int32_t flush) const
  {
    while (true)
    {
      stream_wrapper_->next_out  = reinterpret_cast<decltype(stream_wrapper_->next_out)>(output_buffer_);
      stream_wrapper_->avail_out = buffer_size_;

      const auto code = deflate(stream_wrapper_, flush);
      if (code != Z_OK && code != Z_STREAM_END && code != Z_BUF_ERROR) 
        throw exception(stream_wrapper_, code);
      
      const auto size = stream_buffer_->sputn(output_buffer_, reinterpret_cast<decltype(output_buffer_)>(stream_wrapper_->next_out) - output_buffer_);
      if (size != reinterpret_cast<decltype(output_buffer_)>(stream_wrapper_->next_out) - output_buffer_)
        return -1;

      if (code == Z_STREAM_END || code == Z_BUF_ERROR || size == 0)
        break;
    }
    return 0;
  }

  std::streambuf::int_type overflow    (const std::streambuf::int_type character = traits_type::eof()) override
  {
    stream_wrapper_->next_in  = reinterpret_cast<decltype(stream_wrapper_->next_in)>(pbase());
    stream_wrapper_->avail_in = pptr() - pbase();

    while (stream_wrapper_->avail_in > 0)
    {
      const auto code = deflate_loop(Z_NO_FLUSH);
      if (code != 0)
      {
        setp(nullptr, nullptr);
        return traits_type::eof();
      }
    }

    setp(input_buffer_, input_buffer_ + buffer_size_);
    return traits_type::eq_int_type(character, traits_type::eof()) ? traits_type::eof() : sputc(character);
  }
  std::int32_t             sync        () override
  {
    overflow();

    if (!pptr()) 
      return -1;

    stream_wrapper_->next_in  = nullptr;
    stream_wrapper_->avail_in = 0;

    if (deflate_loop(Z_FINISH) != 0) 
      return -1;

    deflateReset(stream_wrapper_);
    return 0;
  }

private:
  std::streambuf* stream_buffer_ ;
  std::size_t     buffer_size_   ;
  char*           input_buffer_  ;
  char*           output_buffer_ ;
  stream_wrapper* stream_wrapper_;
};

class istream : public std::istream
{
public:
  explicit istream(std::istream&   stream       ) : std::istream(new istreambuf(stream.rdbuf()))
  {
    exceptions(badbit);
  }
  explicit istream(std::streambuf* stream_buffer) : std::istream(new istreambuf(stream_buffer ))
  {
    exceptions(badbit);
  }
  virtual ~istream()
  {
    delete rdbuf();
  }
};
class ostream : public std::ostream
{
public:
  explicit ostream(std::ostream&   stream       ) : std::ostream(new ostreambuf(stream.rdbuf()))
  {
    exceptions(badbit);
  }
  explicit ostream(std::streambuf* stream_buffer) : std::ostream(new ostreambuf(stream_buffer ))
  {
    exceptions(badbit);
  }
  virtual ~ostream()
  {
    delete rdbuf();
  }
};

class ifstream : std::ifstream, public std::istream
{
public:
  explicit ifstream(const std::string& filename, const openmode mode = in) : std::ifstream(filename, mode), std::istream(new istreambuf(rdbuf()))
  {
    exceptions(badbit);
  }
  virtual ~ifstream()
  {
    if (rdbuf()) 
      delete rdbuf();
  }
};
class ofstream : std::ofstream, public std::ostream
{
public:
  explicit ofstream(const std::string& filename, const openmode mode = out) : std::ofstream(filename, mode | binary), std::ostream(new ostreambuf(rdbuf()))
  {
    exceptions(badbit);
  }
  virtual ~ofstream()
  {
    if (rdbuf())
      delete rdbuf();
  }
};
}

#endif